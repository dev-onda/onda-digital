/* Adiciona a imagem do usuario */
$(()=>{

  if(document.getElementById('article')) {
    $('#theme-header').css('display', 'block');
  } else if(!document.getElementById('block-types')){
      $('.highlights-block').css('display', 'none');
  }

  if(document.getElementById('profile-editor-index')) {
    $('.highlights-block').css('top', '-55px');
  }
  var url = window.location.href
  if(url.match("/profile_design")) {
    $(".block-outer").css("height", "400px");
  }
  $(window).scroll(function(){
    if ($(this).scrollTop() > 218) {
      $("#theme-header, #user").addClass("comScroll");
    } else {
      $("#theme-header, #user").removeClass("comScroll");
    }
  });
});
